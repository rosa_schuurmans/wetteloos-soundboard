var audioElement, buttons;

window.onload = function() {
  audioElement = document.querySelector(".fn-audio-elem");
  buttons = document.querySelectorAll(".fn-audio-element");

  var config = {
    apiKey: "AIzaSyDKVeKvFdiWBuhowR-A8txWj87O3r5Z9ow",
    authDomain: "wetteloos-soundboard.firebaseapp.com",
    databaseURL: "https://wetteloos-soundboard.firebaseio.com",
    projectId: "wetteloos-soundboard",
    storageBucket: "wetteloos-soundboard.appspot.com",
    messagingSenderId: "626061905848"
  };

  firebase.initializeApp(config);

  listenToNewData();

  for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function(e) {
      pushNewEvent(e.currentTarget.getAttribute("src"));
    });
  }

  if (navigator.requestMIDIAccess) {
	updateMidiStatusDom("This browser supports WebMIDI!");
    enableMidiSupport(); 
  } else {
    updateMidiStatusDom("WebMIDI is not supported in this browser.");
  }
};

var enableMidiSupport = function() {
  navigator.requestMIDIAccess()
  	.then(
		onMidiSupport, 
		function() { 
			updateMidiStatusDom("Could not access your MIDI devices.")
		}
	);

};

var onMidiSupport = function (midiAccess) {
	updateMidiStatusDom("Got MIDI access! ");
	  
	if(midiAccess.inputs.size === 0) {
	  updateMidiStatusDom("No midi device plugged in atm. Plug & refresh!"); 
	} else {
	  updateMidiStatusDom("Listening to MIDI data ...."); 
	}

	for (var input of midiAccess.inputs.values()) {
		
	  input.onmidimessage = function(message) {
		  var command = message.data[0];
		  var note = message.data[1];
		  var velocity = message.data.length > 2 ? message.data[2] : 0; 

		  if(command === 144) {
			  if(velocity  > 0) {
				  triggerPad(note);
			  } else {
				  unTriggerPad(note);
			  }
			  
		  } else if(command === 128) {
			  unTriggerPad(note);
		  }
		};
	}
}

var updateMidiStatusDom = function(message) {
	var elem = document.createElement('div');
	elem.textContent = message;
	document.querySelector('.fn-midi-status').appendChild(elem);
};

var triggerPad = function(note) {
  var pad = document.querySelectorAll(".fn-audio-element")[note % 12];
  pad.classList.add("active");
  pushNewEvent(pad.getAttribute("src"));
};

var unTriggerPad = function(note) {
  document
    .querySelectorAll(".fn-audio-element")
    [note % 12].classList.remove("active");
};

var listenToNewData = function() {
  var eventsRef = firebase.database().ref("events");

  eventsRef.on("child_added", function(data) {
    var difference = Math.abs(new Date(data.val().date) - new Date());

    if (difference < 1000) {
      playAudio(data.val().src);
    } else if (!data.val().date) {
      playAudio(data.val().src);
    }
  });
};

var playAudio = function(src) {
  audioElement.src = src;
  audioElement.currentTime = 0;
  audioElement.play();

  var playPromise = audioElement.play();
	if (playPromise !== undefined) {
		playPromise.then(function() {
		console.log('started');
		}).catch(function(error) {
			updateMidiStatusDom("Pls click the soundboard first to allow access to your audio usage..."); 
		});

	};

}

var pushNewEvent = function(src) {
  var newEntryKey = firebase
    .database()
    .ref()
    .child("events")
    .push().key;
  var updates = {};
  updates["/events/" + newEntryKey] = {
    date: new Date(),
    id: newEntryKey,
    src: src
  };

  return firebase
    .database()
    .ref()
    .update(updates);
};
